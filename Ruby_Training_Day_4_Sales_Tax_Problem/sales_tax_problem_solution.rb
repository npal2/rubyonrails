# ********* problem statment ***************************
=begin
  Basic sales tax is applicable at a rate of 10% on all goods, except books, food, and medical products that are exempt. Import duty is an additional sales tax applicable to all imported goods at a rate of 5%, with no exemptions.

  When I purchase items I receive a receipt that lists the name of all the items and their price (including tax), finishing with the total cost of the items, and the total amounts of sales taxes paid. The rounding rules for sales tax are that for a tax rate of n%, a shelf price of p contains (np/100 rounded up to the nearest 0.05) amount of sales tax.

  Write an application that prints out the receipt details for a shopping basket.
=end
require 'yaml'

class SalesTaxesProblem
  values = YAML.load_file('constants.yml')
  TAX_EXCEPT_ITEM = %w[chocolates book pills chocolate].freeze
  IMPORT_TAX = values['IMPORT_TAX']
  SALES_TAX = values['SALES_TAX']
  BOTH_TAX = values['BOTH_TAX']
  CENTS_ROUNDING_MULTIPLIER = values['CENTS_ROUNDING_MULTIPLIER']

  def get_input(input_array)
    total_tax = 0
    total_price = 0
    input_array.each do |each_item|
      i_item = each_item.split
      qty = i_item[0].to_i
      price = i_item[-1].to_f
      item = each_item.split(' at ')
      product = item[0].delete('/0-9/').strip
      tax = tax_cal(price, product)
      total_tax += tax
      tax_price = (price.to_f + tax)
      total_price += tax_price
      puts "#{qty} #{product}: #{tax_price.round(2)}"
    end
    puts "Sales Tax: #{total_tax.round(2)}"
    puts "Total: #{total_price.round(2)}"
  end

  def tax_cal(price, product)
    a_product = product.split(' ')
    tax_exclude = a_product & TAX_EXCEPT_ITEM
    if product.include?('imported') and tax_exclude.count != 1
      tax = price.to_f * BOTH_TAX
    elsif product.include?('imported') and tax_exclude.count == 1
      tax = price.to_f * IMPORT_TAX
    elsif tax_exclude.count != 1
      tax = price.to_f * SALES_TAX
    else
      tax = 0
    end
    tax
  end
end

class SalesTaxes
  values = YAML.load_file('constants.yml')
  RECEIPT1 = values['RECEIPT1']
  RECEIPT2 = values['RECEIPT2']
  RECEIPT3 = values['RECEIPT3']
  obj = SalesTaxesProblem.new
  puts '************* First receipt Output **************'
  obj.get_input(RECEIPT1)
  puts '************** Second receipt Output **************'
  obj.get_input(RECEIPT2)
  puts '************** Third receipts Output **************'
  obj.get_input(RECEIPT3)
end

# *********** Possible input **************************
# Input 1:
# 1 book at 12.49
# 1 music CD at 14.99
# 1 chocolate bar at 0.85

# Input 2:
# 1 imported box of chocolates at 10.00
# 1 imported bottle of perfume at 47.50

# Input 3:
# 1 imported bottle of perfume at 27.99
# 1 bottle of perfume at 18.99
# 1 packet of headache pills at 9.75
# 1 box of imported chocolates at 11.25

# *********** Possible Outputs **************************
# Output 1:
# 1 book: 12.49
# 1 music CD: 16.49
# 1 chocolate bar: 0.85
# Sales Taxes: 1.50
# Total: 29.83

# Output 2:
# 1 imported box of chocolates: 10.50
# 1 imported bottle of perfume: 54.65
# Sales Taxes: 7.65
# Total: 65.15

# Output 3:
# 1 imported bottle of perfume: 32.19
# 1 bottle of perfume: 20.89
# 1 packet of headache pills: 9.75
# 1 imported box of chocolates: 11.85
# Sales Taxes: 6.70
# Total: 74.68
