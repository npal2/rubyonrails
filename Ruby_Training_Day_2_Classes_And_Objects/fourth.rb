# ********* problem statment ***************************
# Write a method to double all the elements in an array
# ******************************************************

class DoubleElement
  def double_array(array)
    array.map { |e| e.to_i * 2 }
  end
end

class Fourth
  begin
    object = DoubleElement.new
    puts object.double_array(%w[1 2 3 4])
  rescue StandardError => e
    print 'Expection Occurce -- ', e, '\n'
  end
end
# *********** Possible inputs **************************
# array = %w[1 2 3 4]

# *********** Possible Outputs *************************
=begin
 2
 4
 6
 8
=end
