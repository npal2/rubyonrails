# ********* problem statment ***************************
# Write a program to transpose a N*N matrix.
# ******************************************************

class TranposeMatrix
  def display_and_tranpose(array, displaymatrix)
    array.each_with_index do |row, rowindex|
      row.each_with_index do |col, colindex|
        displaymatrix ? (print col, ' ') : (print array[colindex][rowindex], ' ')
      end
      print "\n"
    end
  end
end

class Eighth
  array = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]]
  begin
    object = TranposeMatrix.new
    puts 'Before tranpose'
    object.display_and_tranpose(array, true)
    puts 'After tranpose'
    object.display_and_tranpose(array, false)
  rescue StandardError => e
    print 'Expection Occurce -- ', e, '\n'
  end
end

# *********** Possible input **************************
# array = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]]

# *********** Possible Outputs **************************
# Before tranpose
# 1 2 3 4
# 5 6 7 8
# 9 10 11 12
# 13 14 15 16
# After tranpose
# 1 5 9 13
# 2 6 10 14
# 3 7 11 15
# 4 8 12 16
