# ********* problem statment ***************************
# Read from a CSV file, multiply two columns and then write back to the CSV file.
# ******************************************************

require 'csv'

class CSV
  def read_file
    @file = CSV.read('demo.csv')
  end

  def write_file
    CSV.open('demo.csv', 'w') do |csv|
      @file.each do |row|
        csv << [row[0].to_i, row[1].to_i, row[0].to_i * row[1].to_i]
      end
    end
  end
end

class Seventh
  begin
    obj = CSV.new
    obj.read_file
    obj.write_file
  rescue StandardError => e
    print 'Expection Occurce -- ', e, '\n'
  end
end

# *********** Possible input **************************
# csv file 
=begin
  1 2
  3 4
  5 6
=end 

# *********** Possible Outputs **************************
=begin
  1 2 2
  3 4 12
  5 6 30
=end 
