# Using a hash table, print the frequency of occurrence of each character inside an array.

class FrequencyElement
  def initialize
    @hash_table = Hash.new(0)
  end

  # method to calculate frequency of array character
  def cal_frequency_of_elements(array)
    array.each do |element|
      @hash_table[element] += 1 unless element.nil?
    end
  end

  # display hash
  def display_hash
    p @hash_table
  end
end

class Sixth
  arr = %w[a b a v a e f e f]
  begin
    obj = FrequencyElement.new
    obj.cal_frequency_of_elements(arr)
    obj.display_hash
  rescue StandardError => e
    print 'Expection Occurce -- ', e, '\n'
  end
end
# *********** Possible inputs **************************
# %w[a b a v a e f e f]
# *********** Possible Outputs *************************
# {"a"=>3, "b"=>1, "v"=>1, "e"=>2, "f"=>2}
