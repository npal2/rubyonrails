# ********* problem statment ***************************
# Join two arrays without using inbuilt functions.
# ******************************************************

class Merge
  def merge_arrays(array1, array2)
    array2.each do |element|
      array1 << element
    end
  end
end

class ThirdProgram 
  array1 = [1, 2, 3]
  array2 = [4, 5, 6]
  begin
    obj = Merge.new
    obj.merge_arrays(array1, array2)
    puts array1
  rescue StandardError => e
    print 'Expection Occurce -- ', e, '\n'
  end
end
# *********** Possible inputs **************************
# [1, 2, 3]
# [4, 5, 6]

# *********** Possible Outputs *************************
# 1 2 3 4 5 6
