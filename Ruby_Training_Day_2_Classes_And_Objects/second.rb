# ********* problem statment ***************************
# Write a program to check if a value exists in an array.
# ******************************************************

class CheckArray
  def initialize(array)
    @array = array
  end

  # check element present in array or not
  def present?(element)
    @array.include? element
  end
end

class SecondProgram
  begin
    object = CheckArray.new([1, 2, 3, 4, 5, 6])
    puts object.present?(gets.chomp.to_i)
  rescue StandardError => e
    print 'Expection Occurce -- ', e, '\n'
  end
end
# *********** Possible inputs **************************
# 4
# *********** Possible Outputs *************************
# true
