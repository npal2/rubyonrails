# Arrays

test_scores = [[97, 76, 79, 93], [79, 84, 76, 79], [88, 67, 64, 76], [94, 55, 67, 81]]

teacher_mailboxes = [['Adams', 'Baker', 'Clark', 'Davis'],['Jones', 'Lewis', 'Lopez', 'Moore'],['Perez', 'Scott', 'Smith', 'Young']
]

# solution 1
puts teacher_mailboxes.first.first
# solution 2
puts teacher_mailboxes[1][0]
# solution 3
puts teacher_mailboxes[2][2]
# solution 4
puts teacher_mailboxes.last.last
# solution 5
puts teacher_mailboxes[1].first(2)
# solution 6
puts teacher_mailboxes[1].last(2)
# solution 7
test_scores.push([100, 99, 98, 97])
puts test_scores
# solution 8
test_scores[0].push(100)
puts test_scores
# solution 9
test_scores[0].delete(100)
puts test_scores
# solution 10
teacher_mailboxes.each_with_index do |items, index|
  print "Row:#{index} = #{items}\n"
end
# solution 11
teacher_mailboxes.each_with_index do |items, row|
  items.each_with_index do |item, col|
    print "Row:#{row} Column:#{col} = #{item}\n"
  end
end
# solution 12
teacher_mailboxes.each_with_index do |row, rowindex|
  row.each_with_index do |col, colindex|
    print "=>#{col} is amazing!\n"
  end
end
# solution 14
test_scores.each do |row|
  row.each do |col|
    puts col > 80 ? true : false
  end
end
# solution 15
test_scores.each do |row|
  row.each do |col|
    puts col if col > 80
  end
end

vehicles = {
  alice: {year: 2019, make: "Toyota", model: "Corolla"},
  blake: {year: 2020, make: "Volkswagen", model: "Beetle"},
  caleb: {year: 2020, make: "Honda", model: "Accord"}
}

# solution 1 
puts vehicles[:alice][:year]
puts vehicles[:blake][:year]
puts vehicles[:caleb][:year]
# solution 2 
# puts vehicles[:zoe][:year]
# solution 3 
# solution 4
vehicles[:dave] = { :year => 2021, :make => "xyz", :model => "abc" } 
puts vehicles
# solution 5
vehicles[:dave] = { :year => 2021, :make => "xyz", :model => "abc", color: "red" } 
puts vehicles
# solution 6
vehicles.delete(:blake)
puts vehicles
# solution 7
vehicles[:dave] = { :year => 2021, :make => "xyz", :model => "abc", color: "red" } 
vehicles[:dave].delete(:color)
puts vehicles
# solution 8
vehicles.each do |key, value|
  puts value if value[:year] >= 2020
end
# # solution 9
vehicles.each do |key, value|
  puts key if value[:year] >= 2020
end
# # solution 10
vehicles.each do |key, value|
  puts key if value[:year] >= 2020 && !value.nil?
end
# solution 11
vehicles.each do |key, value|
  puts key if value[:year] >= 2020 && !value.nil?
end