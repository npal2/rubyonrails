# ********* problem statment ***************************
# Write a program to check if a value exists in an array.
# ******************************************************

# array
array = [1, 2, 3, 4, 5, 6]

# take inputs from the terminal
def take_input
  gets.chomp.to_i
end

# check element present in array or not
def present?(array, element)
  array.include? element
end

puts present?(array, take_input)

# *********** Possible inputs **************************
# 4
# *********** Possible Outputs *************************
# true
