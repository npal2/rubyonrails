# ********* problem statment ***************************
=begin
  Now for the previous question, write another method to double all the elements in the array.
  However, handle edge cases (like array can have a character) as well.
=end
# ******************************************************

# function to double the array elements if it contain character as well
def double_array(array)
  array.map do |element|
    /[0-9]/.match(element).nil? ? element : element.to_i * 2
  end
end

puts double_array(%w[a 2 9 d])

# *********** Possible inputs **************************
# array = ['a', '2','9', 'd'] or %w[a 2 9 d]

# *********** Possible Outputs *************************
=begin
 a
 4
 18
 d
=end
