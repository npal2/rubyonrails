# ********* problem statment ***************************
# Join two arrays without using inbuilt functions.
# ******************************************************

array1 = [1, 2, 3]
array2 = [4, 5, 6]

# function to merge arrays
def merge_arrays(array1, array2)
  array2.each do |element|
    array1 << element
  end
end

merge_arrays(array1, array2)
puts array1

# *********** Possible inputs **************************
# [1, 2, 3]
# [4, 5, 6]

# *********** Possible Outputs *************************
# 1 2 3 4 5 6
