# ********* problem statment ***************************
# Write a program to transpose a N*N matrix.
# ******************************************************

# input array
array = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]]

def display_and_tranpose(array, displaymatrix)
  array.each_with_index do |row, rowindex|
    row.each_with_index do |col, colindex|
      displaymatrix ? (print col, ' ') : (print array[colindex][rowindex], ' ')
    end
    print "\n"
  end
end

puts 'Before tranpose'
display_and_tranpose(array, true)
puts 'After tranpose'
display_and_tranpose(array, false)

# *********** Possible input **************************
# array = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]]

# *********** Possible Outputs **************************
# Before tranpose
# 1 2 3 4
# 5 6 7 8
# 9 10 11 12
# 13 14 15 16
# After tranpose
# 1 5 9 13
# 2 6 10 14
# 3 7 11 15
# 4 8 12 16
