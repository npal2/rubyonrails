# ********* problem statment ***************************
# Write a method to double all the elements in an array
# ******************************************************

# function to double the array elements
def double_array(array)
  array.map { |e| e.to_i * 2 }
end

puts double_array(%w[1 2 3 4])

# *********** Possible inputs **************************
# array = %w[1 2 3 4]

# *********** Possible Outputs *************************
=begin
 2
 4
 6
 8
=end
