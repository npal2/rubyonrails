# ********* problem statment ***************************
# Using a hash table, print the frequency of occurrence of each character inside an array.
# ******************************************************

array = %w[a b a v a e f e f]
hash_table = Hash.new(0)

# method to calculate frequency of array character
def cal_frequency_of_elements(array, hash_table)
  array.each do |element|
    hash_table[element] += 1 unless element.nil?
  end
end

cal_frequency_of_elements(array, hash_table)
puts hash_table

# *********** Possible inputs **************************
# %w[a b a v a e f e f]
# *********** Possible Outputs *************************
# {"a"=>3, "b"=>1, "v"=>1, "e"=>2, "f"=>2}
