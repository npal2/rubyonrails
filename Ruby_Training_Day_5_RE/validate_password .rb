# ********* problem statment ***************************
# 1. To validate Password which includes
#     - Uppercase (A-Z) and lowercase (a-z) English letters.
#     - Digits (0-9).
#     - Characters ! # $ % & ' * + - / = ? ^ _ ` { | } ~
#     - Character. ( period, dot or full stop) provided that it is not the first or last character and it
#       will not come one after the other.
# 2. To validate an Email address
# 3. To validate Credit Card pattern

class RegularExpression
  PASSWORD_REGEX = /^\w(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{5,}$/.freeze
  EMAIL_REGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.freeze
  CREDIT_CARD_REGEX = /^4[0-9]{12}(?:[0-9]{3})?$/.freeze

  def validate_password(password)
    PASSWORD_REGEX.match?(password)
  end

  def validate_email(email)
    EMAIL_REGEX.match?(email)
  end

  def validate_credit_card(number)
    CREDIT_CARD_REGEX.match?(number)
  end
end

obj = RegularExpression.new
puts obj.validate_password('Ahna876n$*shs')
puts obj.validate_email('me-info@example.com')
puts obj.validate_credit_card('765433151286')

# *********** Possible input **************************
# password = Ahna876n$*shs
# email = me-info@example.com
# credit_card = 765433151286

# *********** Possible Outputs **************************
# true
# true
# false
